import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
    todos: []
}

const getters = {
    todos: state => state.todos
}

const actions = {
    gatherTodo({ commit }, data) {
        commit('GATHER_TODO', data)
    },

    setToken({ commit }, token) {
        commit('SET_TOKEN', token)
    },

    deleteToken({ commit }) {
        commit('DELETE_TOKEN')
    }
}

const mutations = {
    GATHER_TODO(state, data) {
        state.todos = data
    },

    SET_TOKEN(token) {
        console.log('tokennya', token)
        localStorage.setItem('token', token)
    },

    DELETE_TOKEN() {
        localStorage.removeItem('token')
    },
}

export default new Vuex.Store({
    state,
    getters,
    actions,
    mutations
})