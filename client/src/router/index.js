import Vue from 'vue'
import Router from 'vue-router'
import Signin from '@/components/signin/signin.vue'
import Signup from '@/components/signup/signup.vue'
import Home from '@/components/home/home.vue'
import Idea from '@/components/idea/idea.vue'

Vue.use(Router)

let token = localStorage.getItem('token')

const guard = (to, from, next) => {
  token ? next() : next({ path: '/signin', query: { redirect: to.fullPath } })
}

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      beforeEnter: (to, from, next) => {
        guard(to, from, next)
      }
    },
    {
      path: '/idea',
      name: 'Idea',
      component: Idea,
      beforeEnter: (to, from, next) => {
        guard(to, from, next)
      }
    },
    {
      path: '/signin',
      name: 'Signin',
      component: Signin
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup
    }
  ]
})
