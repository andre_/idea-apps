Rails.application.routes.draw do
  resources :users do
    resources :ideas do
      resources :todos,
      shallow: true
    end
  end

  get '/ideas', to: 'ideas#index'
  post '/login', to: 'authentications#create'
end
