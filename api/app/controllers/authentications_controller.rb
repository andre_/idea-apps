class AuthenticationsController < ApplicationController
    skip_before_action :authenticate_request

    def create
        command = AuthenticateUser.call(params[:email], params[:password])

        if command.success?
            render json: {
                auth_token: command.result
            }
        else
            render json: {
                errors: command.errors
            },
            status: 401
        end
    end

end
