class UsersController < ApplicationController
    skip_before_action :authenticate_request, except: [:destroy]

    def index
        @users = User.all
        render json: @users
    end

    def create
        @user = User.create(user_params)

        if @user.save
            render json: { success: true, data: @user }
        else
            render json: { success: false }
        end
    end

    def show
        @user = User.find(params[:id])

        render json: { success: true, data: @user }
    end

    def destroy
        @user = User.find(params[:id])

        if @user.destroy
            render json: { success: true }
        else
            render jon: { success: false }
        end
    end

    private
        def user_params
            params.permit(:name, :email, :password, :password_digest)
        end
end
