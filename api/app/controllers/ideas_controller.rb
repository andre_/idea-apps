class IdeasController < ApplicationController
    def index
        @ideas = Idea.all
        render json: @ideas
    end

    def new
        @ideas = Idea.new
    end

    def create
        @user = User.find(params[:user_id])
        @idea = @user.idea.create(idea_params)

        if @idea.save
            render json: @idea
        else
            render 'new'
        end
    end

    private
        def idea_params
            params.permit(:title)
        end
end
