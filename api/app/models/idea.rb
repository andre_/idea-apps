class Idea < ApplicationRecord
    has_many :todo
    belongs_to :user
end
